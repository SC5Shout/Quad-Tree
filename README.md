# Quad Tree

Simple [quadtree](https://en.wikipedia.org/wiki/Quadtree) C++ implementation

- fast
- memory efficient
- lightweight
- easy-to-use

### How to clone
git clone https://gitlab.com/SC5Shout/Quad-Tree.git

### How to build
Quad Tree uses [premake](https://premake.github.io/) to support cross-platform building (but it needs few changes to run not only on Windows)

Everything you need is to run `Win-Gen-Premake.bat` script

It can also be built with CMake. Run the `Win-Gen-Cmake.bat` script or open the working directory in your favourite editor that supports CMake.
It's needed to have CMake installed and added to path environmental variables in order to run the script.

### How to use
Initialization
```cpp
        using namespace QT;

        //initialize the area you want the quad-tree to process
        //x and y - left-top position of the root quad vertex
        //z and w - the width and the height of the root quad   
	Vector4 bounds = {0.0f, 0.0f, 1600.0f, 900.0f};

	uint16_t maxElements = 8, 
	uint8_t maxDepth = 8;
	QuadTree tree{bounds, maxElements, maxDepth};
```

Quad-Tree does not own the object. It just holds the object size and an index into the array the object is in.
```cpp
	using namespace QT;

	struct Particle
	{
		Particle() = default;
		Vector4 rect = 0.0f;
	};

	std::random_device randomDevice;

	Vector4 bounds = { 0.0f, 0.0f, 1600.0f, 900.0f };
	QuadTree tree{ bounds, 8, 8};

	std::vector<Particle> particles(10);
	for(auto& particle : particles) {
		{
			std::uniform_real_distribution<float> distribution(0.0f, 1600.0f);
			particle.rect.x = distribution(randomDevice);
		}

		{
			std::uniform_real_distribution<float> distribution(0.0f, 900.0f);
			particle.rect.y = distribution(randomDevice);
		}

		particle.rect.z = 10.0f;
		particle.rect.w = 10.0f;
	}

	for(uint32_t i = 0; i < 10; ++i) {
		//Insert returns an index, but it is OK to just discard it
		uint32_t j = tree.Insert(i, particles[i].rect);
	}
```

It is very fast to get objects that are inside some area we want to scan.
Query returns a vector of indices into the array that holds the object
```cpp
        using namespace QT;

	struct Particle
	{
		Particle() = default;
		Vector4 rect = 0.0f;
	};

	Vector4 bounds = { 0.0f, 0.0f, 1600.0f, 900.0f };
	QuadTree tree{ bounds, 8, 8};

	std::vector<Particle> particles(10);
	//just test values
	particles[0].rect = {0.0f, 0.0f, 1.0f, 1.0f};
	auto idx0 = tree.Insert(0, particles[0].rect);
        for(uint32_t i = 1; i < 10; ++i) {
		particles[i].rect = {105.0f, 105.0f, 1.0f, 1.0f};
		auto idxI = tree.Insert(i, particles[i].rect);
	}

        //indices array would contain 1 index with value 0 and this value is the index into particles
	Vector4 areaToCheck = { 0.0f, 0.0f, 100.0f, 100.0f };
	std::vector<size_t> indices = tree.Query(areaToCheck);

        for(auto index : indices) {
        	//particle that we found inside the area
		auto& foundParticle = particles[index];
		ProcessParticleThatIsInsideTheArea(foundParticle);
        }
```

To erase the object from the quad-tree, we need an index of this object either from 'QuadTree::Insert' function or from somewhere else
```cpp
        using namespace QT;

	Vector4 bounds = { 0.0f, 0.0f, 1600.0f, 900.0f };
	QuadTree tree{ bounds, 8, 8};

        //                              just example rect values
	size_t object1 = tree.Insert(i, {10.0f, 10.0f, 1.0f, 1.0f});
	size_t object2 = tree.Insert(i, {100.0f, 150.0f, 1000.0f, 500.0f});
	size_t object3 = tree.Insert(i, {432.0f, 532.0f, 884.0f, 365.0f});

        tree.Remove(object2);
	tree.Remove(object1);
	tree.Remove(object3);
```