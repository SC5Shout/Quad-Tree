#include <gtest/gtest.h>
#include <QuadTree.h>

TEST(QuadTree, Vector4_Intersects)
{
	using namespace QT;

	//B is inside of A
	const Vector4 a = {100.0f, 100.0f, 300.0f, 200.0f};
	const Vector4 b = { 200.0f, 200.0f, 400.0f, 300.0f };
	bool result = Intersects(a, b);
	EXPECT_TRUE(result);

	//C intersects with A from left
	const Vector4 c = { 0.0f, 100.0f, 100.0f, 150.0f };
	result = Intersects(a, c);
	EXPECT_TRUE(result);

	//d intersects with A from right
	const Vector4 d = { 300.0f, 100.0f, 400.0f, 200.0f };
	result = Intersects(a, d);
	EXPECT_TRUE(result);

	//e intersects with A from top
	const Vector4 e = { 0.0f, 0.0f, 200.0f, 100.0f };
	result = Intersects(a, e);
	EXPECT_TRUE(result);

	//f intersects with A from bottom
	const Vector4 f = { 100.0f, 200.0f, 300.0f, 300.0f };
	result = Intersects(a, f);
	EXPECT_TRUE(result);

	//g and A do not intersect at all
	const Vector4 g = { 400.0f, 400.0f, 500.0f, 500.0f };
	result = Intersects(a, g);
	EXPECT_FALSE(result);
}

TEST(QuadTree, InsertRemove)
{
	using namespace QT;

	Vector4 bounds = { 0.0f, 0.0f, 1600.0f, 900.0f };
	QuadTree tree{ bounds, 8, 8 };

	size_t object1 = tree.Insert(0, { 10.0f, 10.0f, 1.0f, 1.0f });
	size_t object2 = tree.Insert(1, { 100.0f, 150.0f, 1000.0f, 500.0f });
	size_t object3 = tree.Insert(2, { 432.0f, 532.0f, 884.0f, 365.0f });

	tree.Remove(object3);
	tree.Remove(object1);
	tree.Remove(object2);
}

TEST(QuadTree, Query)
{
	using namespace QT;
	Vector4 bounds = { 0.0f, 0.0f, 1000, 1000 };
	QuadTree tree{ bounds, 8, 8 };

	auto idx0 = tree.Insert(0, { 0.0f, 0.0f, 1.0f, 1.0f });
	for (uint32_t i = 1; i < 10; ++i) {
		auto idx = tree.Insert(i, { 105.0f, 105.0f, 1.0f, 1.0f });
	}

	const Vector4 areaToCheck = { 0.0f, 0.0f, 100.0f, 100.0f };
	const std::vector<size_t> indices = tree.Query(areaToCheck);
	EXPECT_EQ(indices.size(), 1);
	
	const Vector4 areaToCheck2 = { 0.0f, 0.0f, 200.0f, 200.0f };
	const std::vector<size_t> indices2 = tree.Query(areaToCheck2);
	EXPECT_EQ(indices2.size(), 10);

	tree.Remove(idx0);
	const Vector4 areaToCheck3 = { 0.0f, 0.0f, 100.0f, 100.0f };
	const std::vector<size_t> indices3 = tree.Query(areaToCheck3);
	EXPECT_EQ(indices3.size(), 0);

	const std::vector<size_t> indices4 = tree.Query(areaToCheck2);
	EXPECT_EQ(indices4.size(), 9);
}