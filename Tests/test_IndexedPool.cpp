#include <gtest/gtest.h>
#include <IndexedPool.h>

TEST(IndexedPool, Insert)
{
    using namespace QT;
    DynamicIndexedObjectPool<int> pool;

    for (int i = 0; i < 10; ++i) {
        size_t index = pool.insert(10 - i);
        EXPECT_EQ(index, i);

        EXPECT_EQ(pool[i], 10 - i);
    }

    EXPECT_EQ(pool.size(), 10);
}

TEST(IndexedPool, Erase)
{
    using namespace QT;
    DynamicIndexedObjectPool<int> pool;

    size_t index1 = pool.insert(3);
    size_t index2 = pool.insert(343);
    size_t index3 = pool.insert(5233);
    EXPECT_EQ(pool.size(), 3);

    pool.erase(index1);
    EXPECT_EQ(pool.size(), 2);

    pool.erase(index2);
    EXPECT_EQ(pool.size(), 1);

    pool.erase(index3);
    EXPECT_EQ(pool.size(), 0);
}

TEST(IndexedPool, MoveConstructor)
{
    using namespace QT;
    DynamicIndexedObjectPool<int> src;

    size_t index1 = src.insert(3);
    size_t index2 = src.insert(343);
    size_t index3 = src.insert(5233);
    EXPECT_EQ(src.size(), 3);

    DynamicIndexedObjectPool<int> dst{ std::move(src) };

    EXPECT_EQ(src.size(), 0);
    EXPECT_EQ(src.range(), 0);

    EXPECT_EQ(dst.size(), 3);
    EXPECT_EQ(dst.range(), 3);
}

TEST(IndexedPool, MoveOperator)
{
    using namespace QT;
    DynamicIndexedObjectPool<int> src;

    size_t index1 = src.insert(3);
    size_t index2 = src.insert(343);
    size_t index3 = src.insert(5233);
    EXPECT_EQ(src.size(), 3);

    DynamicIndexedObjectPool<int> dst;
    dst = std::move(src);

    EXPECT_EQ(src.size(), 0);
    EXPECT_EQ(src.range(), 0);

    EXPECT_EQ(dst.size(), 3);
    EXPECT_EQ(dst.range(), 3);
}