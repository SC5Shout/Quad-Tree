#include "QuadTree.h"

namespace QT {
    QuadTree::QuadTree(const Vector4& bounds, uint16_t maxElements, uint8_t maxDepth)
        : rootRect(bounds), maxElements(maxElements), maxDepth(maxDepth)
    {
        nodes.emplace_back(LEAF_INDEX, 0);
    }

    size_t QuadTree::Insert(size_t id, const Vector4& rect)
    {
        const size_t newElementIndex = elements.insert(rect, id);

        InsertNode(0, 0, rootRect, newElementIndex);
        return newElementIndex;
    }

    void QuadTree::Remove(size_t elementIndex)
    {
        auto leaves = FindLeaves(0, 0, rootRect, elements[elementIndex].rect);
        for(auto&& [box, nodeIndex, depth] : leaves) {
            size_t index = nodes[nodeIndex].firstChild;
            size_t prevIndex = BRANCH_INDEX;

            while(index != BRANCH_INDEX && elementNodes[index].elementIndex != elementIndex) {
                prevIndex = index;
                index = elementNodes[index].next;
            }

            if(index != BRANCH_INDEX) {
                const size_t nextIndex = index;

                // if prev index is -1 it means that the leaf we're in is the first node of the branch
                // and if we want to remove it from the branch, we need to update the branch's first child
                // to point to an free(empty) leaf
                // else 
                // we're not in the first node of the branch, so we need to update the previous leaf's next to point to the next free(empty) leaf
                if(prevIndex == BRANCH_INDEX) {
                    nodes[nodeIndex].firstChild = nextIndex;
                } else elementNodes[prevIndex].next = nextIndex;

                elementNodes.erase(nodeIndex);
                --nodes[nodeIndex].count;
            }
        }

        elements.erase(elementIndex);
    }

    std::vector<size_t> QuadTree::Query(const Vector4& bounds, size_t omitElementIndex)
    {
        std::vector<size_t> elementsRange;
        elementsRange.reserve(128);

        if(elements.size() > queryCache.size()) {
            queryCache.resize(elements.size());
        }

        auto leaves = FindLeaves(0, 0, rootRect, bounds);
        for(auto&& [box, nodeIndex, depth] : leaves) {
            // get the first child of this leaf's branch
            size_t index = nodes[nodeIndex].firstChild;
            // while there are some leaves to process
            while(index != BRANCH_INDEX) {
                const size_t elementIndex = elementNodes[index].elementIndex;
                const auto& elementRect = elements[elementIndex].rect;
                if(!queryCache[elementIndex] && omitElementIndex != elementIndex && Intersects(elementRect, bounds)) {
                    queryCache[elementIndex] = 1;
                    elementsRange.push_back(elementIndex);
                }

                //search for next leafs in this branch
                index = elementNodes[index].next;
            }
        }

        for(size_t index : elementsRange) {
            queryCache[index] = 0;
        }

        return elementsRange;
    }

    void QuadTree::InsertNode(size_t nodeIndex, uint8_t depth, const Vector4& rootRect, size_t elementIndex)
    {
        // The element that we've just pushed can be that big and occupies more than one leaf
        // so we need to find all leaves that this element needs to be inserted to.
        // Actually `Element` is only inserted once for the whole QuadTree, and an ElementNode is inserted to every leaf that this element occupies
        // and we can refer to this `Element` by using `elementIndex` from `ElementNode`
        auto leaves = FindLeaves(nodeIndex, depth, rootRect, elements[elementIndex].rect);
        for(auto&& leaf : leaves) {
            InsertLeaf(std::move(leaf), elementIndex);
        }
    }

    std::vector<QuadTree::NodeData> QuadTree::FindLeaves(size_t nodeIndex, uint8_t depth, const Vector4& rootRect, const Vector4& elementRect) const
    {
        std::vector<NodeData> toProcess;
        toProcess.reserve(128);
        std::vector<NodeData> result;
        result.reserve(128);

        toProcess.emplace_back(rootRect, nodeIndex, depth);

        while(!toProcess.empty()) {
            NodeData nd = std::move(toProcess.back());
            toProcess.pop_back();

            const size_t index = nd.nodeIndex;
            const uint8_t depth = nd.depth;

            // If this node is a leaf, just give it back
            // else 
            // treat this node as a branch and check its leaves' possible rectangles.
            // If any leaf's rectangle contains the elementRect we need to check also this leaf's subrects, so we push this leaf back to process.
            // The element can occupy more than one leaf, that is why we don't do if() {} else {}, because else may never be executed.
            if(nodes[index].count != LEAF_INDEX) {
                result.push_back(nd);
            } else {
                const size_t firstChild = nodes[index].firstChild;

                const float halfW = nd.rect.z * 0.5f;
                const float halfH = nd.rect.w * 0.5f;

                const float left = nd.rect.x;
                const float right = nd.rect.x + halfW;

                const float top = nd.rect.y;
                const float bot = nd.rect.y + halfH;

                if(elementRect.y <= nd.rect.y + nd.rect.w) {
                    if(elementRect.x <= nd.rect.x + nd.rect.z) {
                        toProcess.emplace_back(Vector4{ right, top, halfW, halfH }, firstChild + 0, depth + 1);
                    }

                    if(elementRect.x + elementRect.z > nd.rect.x) {
                        toProcess.emplace_back(Vector4{ left, top, halfW, halfH }, firstChild + 1, depth + 1);
                    }
                }

                if(elementRect.y + elementRect.w > nd.rect.y) {
                    if(elementRect.x <= nd.rect.x + nd.rect.z) {
                        toProcess.emplace_back(Vector4{ right, bot, halfW, halfH }, firstChild + 2, depth + 1);
                    }

                    if(elementRect.x + elementRect.z > nd.rect.x) {
                        toProcess.emplace_back(Vector4{ left, bot, halfW, halfH }, firstChild + 3, depth + 1);
                    }
                }
            }
        }

        return result;
    }

    void QuadTree::InsertLeaf(NodeData&& leaf, size_t elementIndex)
    {
        const size_t nodeIndex = leaf.nodeIndex;
        const uint8_t depth = leaf.depth;

        nodes[nodeIndex].firstChild = elementNodes.insert(nodes[nodeIndex].firstChild, elementIndex);

        // If the leaf is full, split it and mark as a branch
        // else 
        // increment the leaf's element count
        if(nodes[nodeIndex].count == maxElements && depth < maxDepth) {
            std::vector<size_t> tempElementIndexes;
            tempElementIndexes.reserve(4);

            // This node is a branch but used to be a leaf, so it still contains valid indexes to elements.
            // We need to transfer these indexes to proper leaves.
            // This node's (branch) data is copied to the temporary buffer and removed from the node.
            while(nodes[nodeIndex].firstChild != BRANCH_INDEX) {
                const size_t index = nodes[nodeIndex].firstChild;

                const auto [nextNodeIndex, elementIndex] = elementNodes[index];

                nodes[nodeIndex].firstChild = nextNodeIndex;
                elementNodes.erase(index);

                tempElementIndexes.push_back(elementIndex);
            }

            size_t firstChild = nodes.size();
            nodes.emplace_back(LEAF_INDEX, 0);
            nodes.emplace_back(LEAF_INDEX, 0);
            nodes.emplace_back(LEAF_INDEX, 0);
            nodes.emplace_back(LEAF_INDEX, 0);

            nodes[nodeIndex].firstChild = firstChild;
            nodes[nodeIndex].count = BRANCH_INDEX;

            // For every temporary index we need to insert it into a proper leaf.
            // So, we need to again find leaves that the element rect can occupy and insert it into them.
            // All these steps are already done in InsertNode, so just do it for every element index.
            for(auto&& elementIndex : tempElementIndexes) {
                InsertNode(nodeIndex, depth, rootRect, elementIndex);
            }
        } else ++nodes[nodeIndex].count;
    }
}