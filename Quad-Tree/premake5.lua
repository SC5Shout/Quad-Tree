project "QuadTree"
    kind "StaticLib"
    language "C++"
    cppdialect "C++20"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

    files { 
        "**.h", 
        "**.c", 
        "**.hpp", 
        "**.cpp",
    }

    filter "system:windows"
    systemversion "latest"

    filter "configurations:Debug"
	symbols "On"

    filter "configurations:Release"
    optimize "On"