#pragma once

#include "IndexedPool.h"

namespace QT {
    //TODO modify the code that any custom data type, that has 4 integral/float variables can be used with the quad tree
    struct Vector4
    {
        Vector4(float x, float y, float z, float w)
            : x(x), y(y), z(z), w(w)
        {
        }

        Vector4(float val)
            : x(val), y(val), z(val), w(val)
        {
        }

        float x, y, z, w;
    };

    [[nodiscard]] static inline bool Intersects(const Vector4& a, const Vector4& b)
    {
        const bool leftRight = a.x + a.z >= b.x && a.x <= b.x + b.z;
        const bool botTop = a.y + a.w >= b.y && a.y <= b.y + b.w;
        return leftRight && botTop;
    }

    struct QuadTree
    {
        static constexpr size_t LEAF_INDEX = -1;
        static constexpr int16_t BRANCH_INDEX = -1;

        //consider using uint32_t/int32_t instead of size_t 
        struct Node
        {
            size_t firstChild = 0;
            int16_t count = -1;
        };

        struct Element
        {
            Vector4 rect = 0.0f;
            size_t id = 0;
        };

        struct ElementNode
        {
            size_t next = 0;
            size_t elementIndex = 0;
        };

        struct NodeData
        {
            Vector4 rect = 0.0f;
            size_t nodeIndex = 0;
            uint8_t depth = 0;
        };

        QuadTree(const Vector4& bounds, uint16_t maxElements, uint8_t maxDepth);
        ~QuadTree() = default;

        [[nodiscard]] size_t Insert(size_t id, const Vector4& rect);
        void Remove(size_t elementIndex);

        [[nodiscard]] std::vector<size_t> Query(const Vector4& bounds, size_t omitElement = -1);

    private:
        void InsertNode(size_t nodeIndex, uint8_t depth, const Vector4& rootBox, size_t elementIndex);

        [[nodiscard]] std::vector<NodeData> FindLeaves(size_t nodeIndex, uint8_t depth, const Vector4& rootBox, const Vector4& elementBox) const;
        void InsertLeaf(NodeData&& leaf, size_t element);

        DynamicIndexedObjectPool<Element> elements;
        DynamicIndexedObjectPool<ElementNode> elementNodes;

        std::vector<Node> nodes;
        std::vector<uint8_t> queryCache;

        Vector4 rootRect = 0.0f;

        uint16_t maxElements = 0;
        uint8_t maxDepth = 0;
    };
}

