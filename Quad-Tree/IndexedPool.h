#pragma once

#include <vector>

namespace QT {
    template <typename T>
    struct DynamicIndexedObjectPool
    {
        DynamicIndexedObjectPool()
        {
            pool.reserve(128);
        }

        DynamicIndexedObjectPool(const DynamicIndexedObjectPool& other) = delete;
        DynamicIndexedObjectPool& operator=(const DynamicIndexedObjectPool& other) = delete;

        DynamicIndexedObjectPool(DynamicIndexedObjectPool&& other)
        {
            pool = std::move(other.pool);

            nextFree = other.nextFree;
            other.nextFree = -1;

            m_size = other.m_size;
            other.m_size = 0;
        }

        DynamicIndexedObjectPool& operator=(DynamicIndexedObjectPool&& other)
        {
            if(this == &other) {
                return *this;
            }

            pool = std::move(other.pool);

            nextFree = other.nextFree;
            other.nextFree = -1;

            m_size = other.m_size;
            other.m_size = 0;

            return *this;
        }

        [[nodiscard]] inline size_t insert(const T& element)
        {
            if(nextFree != -1) {
                const size_t index = (size_t)nextFree;
                nextFree = pool[nextFree].next;
                pool[index].element = element;
                ++m_size;
                return index;
            } else {
                pool.emplace_back(element);
                ++m_size;
                return pool.size() - 1;
            }
        }

        [[nodiscard]] inline size_t insert(T&& element)
        {
            if(nextFree != -1) {
                const size_t index = (size_t)nextFree;
                nextFree = pool[nextFree].next;
                pool[index].element = std::move(element);
                ++m_size;
                return index;
            } else {
                pool.emplace_back(std::move(element));

                ++m_size;
                return pool.size() - 1;
            }
        }

        template<typename ... Args>
        [[nodiscard]] inline size_t insert(Args&& ... args)
        {
            return insert(T{ std::forward<Args>(args)... });
        }

        void erase(size_t n)
        {
            pool[n].next = nextFree;
            nextFree = (int64_t)n;
            --m_size;
        }

        void clear()
        {
            pool.clear();
            nextFree = -1;
            m_size = 0;
        }

        [[nodiscard]] inline size_t range() const
        {
            return pool.size();
        }

        [[nodiscard]] inline T& operator[](size_t n)
        {
            return pool[n].element;
        }

        [[nodiscard]] inline const T& operator[](size_t n) const
        {
            return pool[n].element;
        }

        [[nodiscard]] inline size_t size() const
        {
            return m_size;
        }

        [[nodiscard]] inline bool empty() const
        {
            return m_size == 0;
        }

    private:
        union Item
        {
            T element;
            int64_t next;
        };

        std::vector<Item> pool;

        //m_ prefix is not my code-style, but it needs this prefix to avoid collistion with size() method. I want to keep functions in STL style.
        size_t m_size = 0;
        int64_t nextFree = -1;
    };
}

