workspace "QuadTree"
    architecture "x64"
    targetdir "build"

    configurations {
        "Debug",
        "Release"
    }

    flags {       
        "MultiProcessorCompile"
    }

    startproject "QuadTree"

    outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

    include "Quad-Tree"
    include "Tests"